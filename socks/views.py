from pprint import pprint
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User
from django.core.serializers import serialize
from socks.models import Message

def index(request):
	all_users= User.objects.all()
	ctx= {
		"all_users": all_users
	}
	return render(request, "socks/index.html", ctx)




def get_old_messages(request):
	messages= Message.objects.prefetch_related("user").all().order_by("created_at__minute")	
	print(messages)

	serialized_messages = [] #username, message_body, created_at
	for i in messages:
		serialized_messages.append({
			"username": i.user.username,
			"message_body": i.message_body,
			"created_at" : i.created_at
		})


	# return JsonResponse(list(messages), safe=False)
	return JsonResponse(serialized_messages, safe=False)
