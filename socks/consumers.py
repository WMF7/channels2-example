import time
import logging
import json
from asgiref.sync import async_to_sync
from channels.generic.websocket import AsyncWebsocketConsumer, WebsocketConsumer
from channels.consumer import AsyncConsumer
from django.contrib.auth.models import AnonymousUser
from socks.models import Message

logger= logging.getLogger(__name__)

def check_auth(user):
	if(isinstance(user, AnonymousUser)):
		return False
	else:
		return True


class ChatConsumer(WebsocketConsumer):

	payload= {}
	online= []
	

	def connect(self):
		self.user = self.scope["user"]
		print(f"user {self.user.username} connected")
		self.online.append(self.user.username)
		self.accept()
		async_to_sync(self.channel_layer.group_add)("chat", self.channel_name)
		async_to_sync(self.channel_layer.group_send)(
			"chat",
			{
				"text" : json.dumps({
				"online": self.online
				}),
			
			"type": "chat.message"
		})
			
	def disconnect(self, cltimee_code):
		self.online.remove(self.user.username)
		async_to_sync(self.channel_layer.group_send)(
			"chat",
			{
				"text" : json.dumps({
				"online": self.online
				}),
			
			"type": "chat.message"
		})
		
		
		
		
	def receive(self, text_data):
		data = json.loads(text_data)
		Message.objects.create(
			user= self.user,
			message_body= data["message"]
		)
		async_to_sync(self.channel_layer.group_send)(
			"chat",
			{
				"text": json.dumps({
					"message": data["message"] ,
					"username"   : self.user.username,
				}),
				"type": "chat_message"
			},
		)
	

	def chat_message(self, event):
		self.send(text_data= event["text"])


