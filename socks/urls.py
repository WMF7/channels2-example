from django.urls import path
from .views import * 

"""

app_name variable must be provided here,
otherwise it will throw error when including 
this urlspatterns in the main app urls.py

"""
app_name= "socks"

urlpatterns = [
	path("", index, name="index"),
	path("oldmessages", get_old_messages, name="oldmessages")
	
]
