from django.utils.timezone import now
from django.db import models
from django.contrib.auth.models import User


class Message(models.Model):
	message_body= models.TextField()
	user= models.ForeignKey(User, on_delete= models.CASCADE)
	created_at= models.DateTimeField(default=now)
	
